﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {

	public bool move_left;
	public bool move_right;
	public bool able_to_jump;
//	public float acceleration;
	public float maxspeed;
	public float max_height_jump;
	public Healthbar healthbar;
	private float distCovered;
	private bool enemy_touch_left;
	private bool enemy_touch_right;
	public bool knockbacked;
	private bool invincible;
	public float speed;
	public float maxDistance;
	public float maxHeight;
	private float startTime;
	private float save_invicibledelay;
	public float invincibledelay;
	public float time_knock;

	void Start () 
	{
		move_left = false;
		move_right = false;
		able_to_jump = false;
		knockbacked = false;
		invincible = false;
		save_invicibledelay = invincibledelay;
		Physics2D.IgnoreLayerCollision(this.gameObject.layer, LayerMask.NameToLayer("Enemy"), true);
	}

	void OnTriggerEnter2D(Collider2D hit)
	{
		if (hit.gameObject.tag == "ground")
		{
			able_to_jump = true;
		}
	}

	void OnTriggerStay2D(Collider2D hit)
	{
		if (invincible == false)
		{
			if (hit.gameObject.tag == "enemy_hitbox")
			{
				if (hit.gameObject.transform.position.x < this.transform.position.x)
				{
					enemy_touch_left = true;
					enemy_touch_right = false;
				}
				else
				{
					enemy_touch_left = false;
					enemy_touch_right = true;
				}
				if (hit.gameObject.transform.parent.name == "EnemyDistant")
					healthbar.AdjustHealth(-hit.gameObject.transform.parent.GetComponent<EnemyDistant>().damage);
				else
					healthbar.AdjustHealth(-hit.gameObject.transform.parent.GetComponent<Ennemy>().damage);
				KnockBack();
			}
		}
	}

	void OnCollisionEnter2D(Collision2D hit)
	{
		if (hit.gameObject.tag == "ground")
		{
			able_to_jump = true;
		}
	}


	void KnockBack ()
	{	
		knockbacked = true;
		startTime = Time.time;
		invincible = true;
	}

	void FixedUpdate ()
	{
		if (knockbacked == false)
		{
			Vector2 currentVelocity = this.rigidbody2D.velocity;
			Vector2 direction = new Vector2(Input.GetAxis("Horizontal"), 0);

			currentVelocity.x = maxspeed * direction.normalized.x;
			this.rigidbody2D.velocity = currentVelocity;
		}
		if (enemy_touch_left)
		{
			enemy_touch_left = false;
			float g = Physics.gravity.magnitude; // get the gravity value
			float vSpeed = Mathf.Sqrt(2 * g * maxHeight); // calculate the vertical speed
			float totalTime = 2 * vSpeed / g; // calculate the total time
			float hSpeed = maxDistance / totalTime; // calculate the horizontal speed
			this.rigidbody2D.velocity = new Vector2(hSpeed, vSpeed); // launch the projectile!
		}
		else if (enemy_touch_right)
		{
			enemy_touch_right = false;
			float g = Physics.gravity.magnitude; // get the gravity value
			float vSpeed = Mathf.Sqrt(2 * g * maxHeight); // calculate the vertical speed
			float totalTime = 2 * vSpeed / g; // calculate the total time
			float hSpeed = maxDistance / totalTime; // calculate the horizontal speed
			this.rigidbody2D.velocity = new Vector2(- hSpeed, vSpeed); // launch the projectile!
		}
	}

	void Update()
	{
		if (!knockbacked && able_to_jump)
		{
			if ((Input.GetKeyDown(KeyCode.Space) || Input.GetButtonDown("Jump")))
			{
				this.rigidbody2D.velocity = new Vector2(this.rigidbody2D.velocity.x, max_height_jump);
				able_to_jump = false;
			}
		}

		if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetAxis("Horizontal") < 0)
		{
			move_right = true;
			move_left = false;
		}
		
		if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetAxis("Horizontal") > 0)
		{
			move_right = false;
			move_left = true;
		}
		
		if (Input.GetKeyUp(KeyCode.RightArrow) || Input.GetAxis("Horizontal") == 0)
			move_right = false;
		
		if (Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetAxis("Horizontal") == 0)
			move_left = false;

		if (invincible)
		{
			invincibledelay -= Time.deltaTime;
		}
		if (invincibledelay <= 0)
		{
			invincible = false;
			invincibledelay = save_invicibledelay;
		}
		if (knockbacked)
		{			
			if (Time.time - startTime >= time_knock)
			{
				knockbacked = false;
			}
		}
	}
}