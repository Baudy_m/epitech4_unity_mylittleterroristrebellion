﻿using UnityEngine;
using System.Collections;

public class Ennemy : MonoBehaviour
{
	private GameObject player;
	private Animator enemyAnimator;
	public float speed;
	public int damage;
	public int pv;
	public int distanceDetection;
	private bool playerRightDirection;
	private bool walk;


	void Start ()
	{
		this.walk = false;
//		Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer ("Enemy"), LayerMask.NameToLayer ("ground"), false);

		Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer ("Enemy"), LayerMask.NameToLayer ("Enemy"));
		player = GameObject.Find ("Player");
		this.enemyAnimator = GetComponentInChildren<Animator> ();
		if (Mathf.Sign (player.transform.position.x - this.transform.position.x) > 0) {
			this.transform.rotation = new Quaternion (0f, 180f, 0f, 0f);
			playerRightDirection = true;
		} else if (Mathf.Sign (player.transform.position.x - this.transform.position.x) < 0) {
			this.transform.rotation = new Quaternion (0f, 0f, 0f, 0f);
			playerRightDirection = false;
		}
	}

	void FixedUpdate ()
	{
		Vector2 enemyVelocity = this.rigidbody2D.velocity;
		if (this.walk) {
			enemyVelocity.x = Mathf.Sign (player.transform.position.x - this.transform.position.x) * speed;
			this.rigidbody2D.velocity = enemyVelocity;
		}
		else 
		{						
			enemyVelocity.x = 0;
			this.rigidbody2D.velocity = enemyVelocity;
		}
		//print ("ENEMY.velocity => " + this.rigidbody2D.velocity.ToString ());
	}

	void Update ()
	{
		if (Mathf.Abs (player.transform.position.x - this.transform.position.x) > this.distanceDetection) {
			if (this.walk)
				this.enemyAnimator.SetBool ("Walk", false);
			this.walk = false;
		} else {
			if (!this.walk)
				this.enemyAnimator.SetBool ("Walk", true);
			this.walk = true;
		}


		if (!playerRightDirection && Mathf.Sign (player.transform.position.x - this.transform.position.x) > 0) {
			this.transform.Rotate (0, 180, 0);
			this.playerRightDirection = true;
		} else if (playerRightDirection && Mathf.Sign (player.transform.position.x - this.transform.position.x) < 0) {
			this.transform.Rotate (0, 180, 0);
			this.playerRightDirection = false;
		}
	}
}
