﻿using UnityEngine;
using System.Collections;

public class Ennemy_movement_fun : MonoBehaviour {

	// Use this for initialization

	public GameObject character;

	private Vector2 save_charac_position;
	private Vector2 save_my_position;

	private float progress;

	private bool joined;

	void Start () 
	{
		progress = 0;
		save_my_position = this.transform.position;
		save_charac_position = character.transform.position;
		joined = false;
		print (save_charac_position);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (progress < 1 && !joined)
		{
			this.transform.position = Vector2.Lerp(save_my_position, save_charac_position, progress);
			progress += Time.deltaTime;
		}
		else
		{
			progress = 0;
			joined = true;
		}

		if (save_charac_position != (Vector2)character.transform.position)
		{
			save_my_position = this.transform.position;
			save_charac_position = character.transform.position;
			progress = 0;
			joined = false;
		}
	}
}
