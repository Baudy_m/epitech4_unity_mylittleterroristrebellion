﻿using UnityEngine;
using System.Collections;

public class MainTitle : MonoBehaviour {

	// Use this for initialization
	public GUIStyle styleButtonHover = null;
	public string soundeffectsStatus = "ON";
	public string musicStatus = "ON";
	public bool soundeffects = true;
	public bool options = false;
	public bool musics = true;
	public GUISkin skin;

	public bool up = false;
	public bool down = false;
	public bool change = false;

	public string[] buttonNames;
//	public string[] buttonNames = {"New Game", "Continue", "Options", "Quit Game"};
	public string[] buttonOptionsNames = {"Sounds", "Musics", "Back"};
	public bool[] buttons;
	public bool[] buttonsOptions;
	public int currentSelection = 0;
	public int currentSelectionOptions = 0;

	public AudioClip backgroundMusic;
	public AudioClip soundMenu;
	
	void Start()
	{
		buttons = new bool[buttonNames.Length];
		buttonsOptions = new bool[buttonOptionsNames.Length];
		PlayerPrefs.SetInt ("sounds", 1);
		PlayerPrefs.SetInt ("musics", 1);
	}

	void Update()
	{
		if (Input.GetAxisRaw("Vertical") == -1 && change == false)
		{
			up = false;
			change = true;
			down = true;
		}
		if (Input.GetAxisRaw("Vertical") == 1 && change == false)
		{
			up = true;
			down = false;
			change = true;
		}
		if (Input.GetAxisRaw ("Vertical") == 0 && Input.GetAxis ("Horizontal") == 0)
			change = false;
		if (options)
		{
			if (up && !change)
			{
				currentSelectionOptions -= 1;
				if (currentSelectionOptions < 0)
					currentSelectionOptions = 2;
				GUI.FocusControl(buttonOptionsNames[currentSelectionOptions]);
				//audio.playoneshot(sound);
				up = false;
			}
			if (down && !change)
			{
				currentSelectionOptions += 1;
				if (currentSelectionOptions > 2)
					currentSelectionOptions = 0;
				GUI.FocusControl(buttonOptionsNames[currentSelectionOptions]);
				down = false;
			}
			if (Input.GetKeyDown (KeyCode.Joystick1Button0))
			{
				// When the use key is pressed, the selected button will activate
				buttonsOptions[currentSelectionOptions] = true;
			}
		}
		else
		{
			if (up && !change)
			{
				currentSelection -= 1;
				if (currentSelection < 0)
					currentSelection = 3;
				GUI.FocusControl(buttonNames[currentSelection]);
				up = false;
			}
			if (down && !change)
			{
				currentSelection += 1;
				if (currentSelection > 3)
					currentSelection = 0;
				GUI.FocusControl(buttonNames[currentSelection]);
				down = false;
			}
			if (Input.GetKeyDown (KeyCode.Joystick1Button0))
			{
				// When the use key is pressed, the selected button will activate
				buttons[currentSelection] = true;
			}
		}
	}

	void OnGUI ()
	{
		GUI.skin = skin;
		GUI.skin.customStyles [1].normal.textColor = Color.black;
		GUI.skin.button.normal.textColor = Color.black;
		if (options)
			displayOptionsMenu();
		else
			displayMenu ();
	}

	void displayMenu()
	{
		if (buttons [0]) 
		{
			if (soundeffects)
				audio.PlayOneShot(soundMenu);
			Application.LoadLevel ("FirstStage");// 
		}
		if (buttons[1]) {
			if (soundeffects)
				audio.PlayOneShot(soundMenu);
			Application.LoadLevel (Application.loadedLevelName); //
		}
		if (buttons[2]) {
			if (soundeffects)
				audio.PlayOneShot(soundMenu);
			options = true; // Go to the options
		}
		if (buttons[3]) {
			if (soundeffects)
				audio.PlayOneShot(soundMenu);
			Application.Quit(); // Back to the Main Title
		}
		for (int i = 0; i < buttonNames.Length; i++)
		{
			GUI.SetNextControlName (buttonNames [i]);
			if (i == currentSelection)
				buttons [i] = GUI.Button (new Rect (Screen.width / 4.31f, (Screen.height / 2.18f) + (i * 50), Screen.width / 1.94f, Screen.height / 10.9f), buttonNames [i], styleButtonHover);
			else
				buttons [i] = GUI.Button (new Rect (Screen.width / 4.31f, (Screen.height / 2.18f) + (i * 50), Screen.width / 1.94f, Screen.height / 10.9f), buttonNames [i]);
		}		
		GUI.Label (new Rect (Screen.width / 4.31f, Screen.height / 14.53f, Screen.width / 1.94f, Screen.height / 10.9f), "My little terrorist \n Rebellion", "Title");
	}
	
	void displayOptionsMenu()
	{
		if (buttonsOptions [0])
		{
			soundeffects = !soundeffects;
			if (soundeffects)
			{
				soundeffectsStatus = "ON";
				PlayerPrefs.SetInt("sounds", 1);
				audio.PlayOneShot(soundMenu);
			}
			else
			{
				soundeffectsStatus = "OFF";
				PlayerPrefs.SetInt("sounds", 0);
			}
		}
		if (buttonsOptions [1])
		{
			if (soundeffects)
				audio.PlayOneShot(soundMenu);
			musics = !musics;
			if (musics)
			{
				musicStatus = "ON";
				PlayerPrefs.SetInt("musics", 1);
				audio.Play();
			}
			else
			{
				musicStatus = "OFF";
				PlayerPrefs.SetInt("musics", 0);
				audio.Stop();
			}
		}
		if (buttonsOptions [2])
		{
			if (soundeffects)
				audio.PlayOneShot(soundMenu);
			options = false;
		}
		for (int i = 0; i < buttonOptionsNames.Length; i++)
		{
			GUI.SetNextControlName (buttonOptionsNames [i]);
			if (i == 2 && i != currentSelectionOptions)
				buttonsOptions [i] = GUI.Button (new Rect ((Screen.width / 2) - 85, 550, 150, 25), buttonOptionsNames [i]);
			else if (i == 2 && i == currentSelectionOptions)
				buttonsOptions [i] = GUI.Button (new Rect ((Screen.width / 2) - 85, 550, 150, 25), buttonOptionsNames [i], styleButtonHover);
			else 
			{
				if (i == currentSelectionOptions)
					buttonsOptions [i] = GUI.Button (new Rect (400, 300 + (i * 55), 150, 25), buttonOptionsNames [i], styleButtonHover);
				else
					buttonsOptions [i] = GUI.Button (new Rect (400, 300 + (i * 55), 150, 25), buttonOptionsNames [i]);
			}
		}
		if (Input.GetAxisRaw("Vertical") == -1 && change == false)
		{
			up = false;
			change = true;
			down = true;
		}
		if (Input.GetAxisRaw("Vertical") == 1 && change == false)
		{
			up = true;
			down = false;
			change = true;
		}
		if (Input.GetAxisRaw ("Vertical") == 0 && Input.GetAxis ("Horizontal") == 0)
			change = false;
		GUI.Label (new Rect (Screen.width / 4.31f, Screen.height / 14.53f, Screen.width / 1.94f, Screen.height / 10.9f), "Options", "Title");
		GUI.Label (new Rect (770, 300, 150, 25), soundeffectsStatus, "label");
		GUI.Label (new Rect (770, 355, 150, 25), musicStatus, "label");
	}

}
