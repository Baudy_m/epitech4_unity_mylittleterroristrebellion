﻿using UnityEngine;
using System.Collections;

public class HUD : MonoBehaviour {

	public GUISkin skin;
	public Texture2D[] items;
	private bool musicOff = false;

	// Use this for initialization
	void Start () {
		items = new Texture2D[3];
		items[0] = (Texture2D)Resources.Load("TestWeapon");
		if (PlayerPrefs.GetInt("musics") == 0)//|| !GetComponent<Menu> ().musics)
			audio.Stop ();
		else
			audio.Play ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (GetComponent<GameOver> ().gameOver)
			if (!musicOff)
			{
				//audio.Stop ();
				audio.clip = GetComponent<GameOver> ().backgroundMusic;
				musicOff = true;
			}
		if (Input.GetKeyDown (KeyCode.I))
		{
			for (int i  = 0; i <= 2; i++)
			{
				if (!items[i])
				{
					items[i] = (Texture2D)Resources.Load("TestWeapon");
					break;
				}
			}
		}
	}

	void OnGUI()
	{
		GUI.skin = skin;
		if (items[0])
			GUI.Box (new Rect (200, 500, 100, 100), items[0], "box");
		else
			GUI.Box (new Rect (200, 500, 100, 100), "", "box");
		if (items[1])
			GUI.Box (new Rect (350, 500, 100, 100), items[1], "box");
		else 
			GUI.Box (new Rect (350, 500, 100, 100), "",  "box");
		if (items[2])
			GUI.Box (new Rect (500, 500, 100, 100), items[2], "box");
		else 
			GUI.Box (new Rect (500, 500, 100, 100), "",  "box");

	}
}
