﻿using UnityEngine;
using System.Collections;

public class GameOver : MonoBehaviour {
	
	public bool gameOver = false;
	private float y;
	private float timeMove;
	public string[] buttonNames = {"Try Again", "Main Title"};
	public GUIStyle styleButtonHover = null; // Style du bouton
	public bool[] buttons;
	public GUISkin skin;
	private bool left = false;
	private bool right = false;
	private bool change = false;
	private int currentSelection = 0;

	public AudioClip backgroundMusic;
	public AudioClip soundMenu;
	private bool musicOn = false;

	// Use this for initialization
	void Start () 
	{
		buttons = new bool[buttonNames.Length];
		y = -50f;
		timeMove = 0.0f;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (GameObject.Find("Player").GetComponent<Healthbar>().curhealth <= 0)
			gameOver = true;
		if (gameOver)
		{
			if (!musicOn)
			{
				audio.clip = backgroundMusic;
				audio.loop = true;
				audio.Play();
				musicOn = true;
			}

			if (Input.GetAxisRaw("Horizontal") == -1 && change == false)
			{
				left = true;
				change = true;
				right = false;
			}
			if (Input.GetAxisRaw("Horizontal") == 1 && change == false)
			{
				left = false;
				right = true;
				change = true;
			}
			if (Input.GetAxisRaw ("Vertical") == 0 && Input.GetAxis ("Horizontal") == 0)
				change = false;
			if (left && !change)
			{
				currentSelection = 0;
				GUI.FocusControl(buttonNames[0]);
				left = false;
			}
			if (right && !change)
			{
				currentSelection = 1;
				GUI.FocusControl(buttonNames[1]);
				right = false;
			}
			if (Input.GetKeyDown (KeyCode.Joystick1Button0))
			{
				// When the use key is pressed, the selected button will activate
				buttons[currentSelection] = true;
			}
		}
	}

	void OnGUI()
	{
		GUI.skin = skin;
		if (gameOver)
		{
			timeMove += Time.deltaTime;
			if (timeMove <= 3)
			{
				y += 1 * timeMove;
				GUI.Label (new Rect (Screen.width / 4.31f, y, Screen.width / 1.94f, Screen.height / 10.9f), "Game Over", "Title");
			}
			if (timeMove > 3)
			{
				GUI.Label (new Rect (Screen.width / 4.31f, y, Screen.width / 1.94f, Screen.height / 10.9f), "Game Over", "Title");
				Time.timeScale = 0f;

				if (buttons[0]) {
					if (GetComponent<Menu> ().soundeffects)
						audio.PlayOneShot(soundMenu);
					Application.LoadLevel (Application.loadedLevelName);
				}
				if (buttons[1]) {
					if (GetComponent<Menu> ().soundeffects)
						audio.PlayOneShot(soundMenu);
					Application.LoadLevel ("MainTitle"); // Back to the Main Title
				}

				for (int i = 0; i < buttonNames.Length; i++)
				{
					GUI.SetNextControlName (buttonNames [i]);
					if (i == currentSelection)
						buttons [i] = GUI.Button (new Rect (150 + (i * 350), 400 , Screen.width / 1.94f, Screen.height / 10.9f), buttonNames [i], styleButtonHover);
					else
						buttons [i] = GUI.Button (new Rect (150 + (i * 350), 400 , Screen.width / 1.94f, Screen.height / 10.9f), buttonNames [i]);
				}
			}
		}
	}
}
