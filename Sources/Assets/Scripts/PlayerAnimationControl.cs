﻿using UnityEngine;

public class PlayerAnimationControl : MonoBehaviour
{
    private Animator animator;
    private Animator[] animatorChilds;
    private int currentHash;
    private SpriteRenderer[] spriteRendereChilds;
	public bool flip = false;

    // Use this for initialization
    private void Start()
    {
        animator = GetComponent<Animator>();
        animatorChilds = GetComponentsInChildren<Animator>();
        spriteRendereChilds = GetComponentsInChildren<SpriteRenderer>();
        for (int i = 0; i < spriteRendereChilds.Length; ++i)
        {
            //Debug.Log("SpriteRenderer[" + i + "].name = " + spriteRendereChilds[i].name);

            if (spriteRendereChilds[i].name.Contains("Eye") && !spriteRendereChilds[i].name.Contains("White"))
                spriteRendereChilds[i].color = Color.cyan;
            else if (spriteRendereChilds[i].name.Contains("Hair"))
                spriteRendereChilds[i].color = Color.yellow;
            else if (spriteRendereChilds[i].name.Contains("Head") || spriteRendereChilds[i].name.Contains("Hand"))
                spriteRendereChilds[i].color = Color.grey;
            else if (spriteRendereChilds[i].name.Contains("LowerBody"))
                spriteRendereChilds[i].color = Color.red;
            else if (spriteRendereChilds[i].name.Contains("UpperBody"))
                spriteRendereChilds[i].color = Color.green;
        }
    }

	public void test()
	{
		print ("TEST");
	}

    // Update is called once per frame
    private void Update()
    {
        if (currentHash != animator.GetCurrentAnimatorStateInfo(0).nameHash)
        {
            animatorChilds = GetComponentsInChildren<Animator>();
            currentHash = animator.GetCurrentAnimatorStateInfo(0).nameHash;
//            Debug.Log("StateInfo.nameHash = " + currentHash);
//            for (int i = 0; i < animatorChilds.Length; ++i)
//                Debug.Log("Animator[" + i + "].name = " + animatorChilds[i].name);
        }
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = 10.0f;
        Vector3 lookPos = Camera.main.ScreenToWorldPoint(mousePos);
        lookPos = lookPos - transform.position;
        float angle = Mathf.Atan2(lookPos.y, lookPos.x) * Mathf.Rad2Deg;

        //				transform.rotation = Quaternion.AngleAxis (angle, Vector3.forward);

        if (Input.GetKey(KeyCode.UpArrow))
        {
            for (int i = 0; i < animatorChilds.Length; ++i)
            {
                animatorChilds[i].speed += 1.0f * Time.deltaTime;
                if (animatorChilds[i].speed > 10.0f)
                    animatorChilds[i].speed = 10.0f;
            }
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            for (int i = 0; i < animatorChilds.Length; ++i)
            {
                animatorChilds[i].speed -= 1.0f * Time.deltaTime;
                if (animatorChilds[i].speed < 1.0f)
                    animatorChilds[i].speed = 1.0f;
            }
        }

		if (angle > -90 && angle < 90)
			flip = false;
		else if (angle < -90 || angle > 90)
			flip = true;


		if (Input.GetAxis("Horizontal") > 0)
			flip = false;
		else if (Input.GetAxis("Horizontal") < 0)
			flip = true;

        if ( !flip)
            transform.rotation = new Quaternion(0, 0f, 0, 0);
        else
            transform.rotation = new Quaternion(0, 180f, 0, 0);

		if (Input.GetMouseButtonDown(0) || Input.GetButtonDown("Fire3"))
            animator.SetBool("AttackMelee", true);
		else if (Input.GetMouseButtonUp(0) || Input.GetButtonUp("Fire3"))
            animator.SetBool("AttackMelee", false);

		if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.Space) || Input.GetButton("Jump"))
            animator.SetBool("Jump", true);
        else
            animator.SetBool("Jump", false);

        if (Input.GetKey(KeyCode.A) || Input.GetAxis("Horizontal") < 0)
            animator.SetBool("Walk", true);
		else if (Input.GetKey(KeyCode.D) || Input.GetAxis("Horizontal") > 0)
            animator.SetBool("Walk", true);
        else
            animator.SetBool("Walk", false);
    }
}