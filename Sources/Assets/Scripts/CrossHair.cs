﻿using UnityEngine;
using System.Collections;

public class CrossHair : MonoBehaviour {

	public GameObject player;

	// Use this for initialization
	void Start () {
		Vector3 mousePos = Input.mousePosition;
		mousePos.z = 0.0f;
		Vector3 lookPos = Camera.main.ScreenToWorldPoint(mousePos);
		lookPos = lookPos - this.player.transform.position;
		float angle = Mathf.Atan2(lookPos.y, lookPos.x);
		this.transform.position = new Vector2((Mathf.Cos(angle) * 1.0f - Mathf.Sin(angle) * 0.0f) * 3f + this.transform.parent.position.x,
		                                      (Mathf.Cos(angle) * 0.0f + Mathf.Sin(angle) * 1.0f) * 3f + this.transform.parent.position.y);
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 mousePos = Input.mousePosition;
		mousePos.z = 0.0f;
		Vector3 lookPos = Camera.main.ScreenToWorldPoint(mousePos);
		lookPos = lookPos - this.player.transform.position;
		float angle = Mathf.Atan2(lookPos.y, lookPos.x);
		this.transform.position = new Vector2((Mathf.Cos(angle) * 1.0f - Mathf.Sin(angle) * 0.0f) * 3f + this.transform.parent.position.x,
		                                      (Mathf.Cos(angle) * 0.0f + Mathf.Sin(angle) * 1.0f) * 3f + this.transform.parent.position.y);
	}
}
