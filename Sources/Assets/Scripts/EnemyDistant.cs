﻿using UnityEngine;
using System.Collections;

public class EnemyDistant : MonoBehaviour
{

		private GameObject player;
		private GameObject bullet;
		private Animator enemyAnimator;
		public GameObject originalSpell;
		public float speed;
		public float bulletSpeed;
		public int damage;
		public int pv;
		public int distanceDetection;
		public int distanceFiring;
		private Vector2 targetDirection;
		private float targetAngle;
		private bool playerRightDirection;
		private bool walk;
		private bool attacking;
	
		void Start ()
		{
				this.attacking = false;
				this.walk = false;
				//		Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer ("Enemy"), LayerMask.NameToLayer ("ground"), false);
		
				Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer ("Enemy"), LayerMask.NameToLayer ("Enemy"));
				player = GameObject.Find ("Player");
				this.enemyAnimator = GetComponentInChildren<Animator> ();
				if (Mathf.Sign (player.transform.position.x - this.transform.position.x) > 0) {
						this.transform.rotation = new Quaternion (0f, 0f, 0f, 0f);
						playerRightDirection = true;
				} else if (Mathf.Sign (player.transform.position.x - this.transform.position.x) < 0) {
						this.transform.rotation = new Quaternion (0f, 180f, 0f, 0f);
						playerRightDirection = false;
				}
		}
	
		void FixedUpdate ()
		{
				Vector2 enemyVelocity = this.rigidbody2D.velocity;
				if (this.walk && !this.attacking) {
						enemyVelocity.x = Mathf.Sign (player.transform.position.x - this.transform.position.x) * speed;
						this.rigidbody2D.velocity = enemyVelocity;
				} else {
						enemyVelocity.x = 0;
						this.rigidbody2D.velocity = enemyVelocity;
				}
				//print ("ENEMY.velocity => " + this.rigidbody2D.velocity.ToString ());
		}
	
		void Update ()
		{
				
				if (Mathf.Abs (player.transform.position.x - this.transform.position.x) > this.distanceDetection) {
						this.walk = false;
				} else {
						this.walk = true;
				}


				if (!this.attacking && 
						(Mathf.Abs (player.transform.position.x - this.transform.position.x) <= this.distanceFiring)) {
						this.enemyAnimator.SetBool ("Attack", true);
						this.attacking = true;
				}

				if (this.attacking) {
						this.targetDirection = player.transform.position - this.transform.position;
						this.targetAngle = Mathf.Atan2 (targetDirection.y, targetDirection.x);
						this.targetDirection.x = (Mathf.Cos (targetAngle) * 1.0f - Mathf.Sin (targetAngle) * 0.0f) * 1f;
						this.targetDirection.y = (Mathf.Cos (targetAngle) * 0.0f + Mathf.Sin (targetAngle) * 1.0f) * 1f;
						
				}

		
				if (!playerRightDirection && Mathf.Sign (player.transform.position.x - this.transform.position.x) > 0) {
						this.transform.Rotate (0, 180, 0);
						this.playerRightDirection = true;
				} else if (playerRightDirection && Mathf.Sign (player.transform.position.x - this.transform.position.x) < 0) {
						this.transform.Rotate (0, 180, 0);
						this.playerRightDirection = false;
				}
		}

		void Shoot ()
		{
				bullet = Instantiate (this.originalSpell, this.transform.position, Quaternion.identity) as GameObject;
				EnemyBullet bulletcollider = bullet.GetComponent<EnemyBullet> ();
				bulletcollider.speed = this.bulletSpeed;
				bulletcollider.direction = targetDirection;
				this.targetDirection.x = (Mathf.Cos (targetAngle) * 1.0f - Mathf.Sin (targetAngle) * 0.0f) * 1f;
				this.targetDirection.y = (Mathf.Cos (targetAngle) * 0.0f + Mathf.Sin (targetAngle) * 1.0f) * 1f;
				this.targetAngle *= Mathf.Rad2Deg;
				bulletcollider.angle = this.targetAngle;
				bulletcollider.transform.Rotate (0, 0, this.targetAngle);

				this.targetAngle += 10;
				this.targetAngle *= Mathf.Deg2Rad;
				this.targetDirection.x = (Mathf.Cos (targetAngle) * 1.0f - Mathf.Sin (targetAngle) * 0.0f) * 1f;
				this.targetDirection.y = (Mathf.Cos (targetAngle) * 0.0f + Mathf.Sin (targetAngle) * 1.0f) * 1f;
				this.targetAngle *= Mathf.Rad2Deg;

				bullet = Instantiate (this.originalSpell, this.transform.position, Quaternion.identity) as GameObject;
				bulletcollider = bullet.GetComponent<EnemyBullet> ();
				bulletcollider.speed = this.bulletSpeed;
				bulletcollider.direction = targetDirection;
				bulletcollider.angle = this.targetAngle;
				bulletcollider.transform.Rotate (0, 0, this.targetAngle);

				this.targetAngle -= 20;
				this.targetAngle *= Mathf.Deg2Rad;
				this.targetDirection.x = (Mathf.Cos (targetAngle) * 1.0f - Mathf.Sin (targetAngle) * 0.0f) * 1f;
				this.targetDirection.y = (Mathf.Cos (targetAngle) * 0.0f + Mathf.Sin (targetAngle) * 1.0f) * 1f;
				this.targetAngle *= Mathf.Rad2Deg;
				
				bullet = Instantiate (this.originalSpell, this.transform.position, Quaternion.identity) as GameObject;
				bulletcollider = bullet.GetComponent<EnemyBullet> ();
				bulletcollider.speed = this.bulletSpeed;
				bulletcollider.direction = targetDirection;
				bulletcollider.angle = this.targetAngle;
				bulletcollider.transform.Rotate (0, 0, this.targetAngle);
		}

		void EndAttack ()
		{
				this.attacking = false;
				this.enemyAnimator.SetBool ("Attack", false);
		}
}
