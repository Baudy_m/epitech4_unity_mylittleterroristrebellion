﻿using UnityEngine;
using System.Collections;

public class Follow_Player : MonoBehaviour {

	// Use this for initialization

	public GameObject character;

	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		this.transform.position = new Vector3(character.transform.position.x,
		                                      this.transform.position.y,
		                                      this.transform.position.z);
	}
}
