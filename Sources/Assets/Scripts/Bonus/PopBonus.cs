﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PopBonus : MonoBehaviour {

	// Use this for initialization

	public List<GameObject> bonus;

	public bool pop;
	private int range;

	void Start () 
	{
		pop = false;
		range = 0;
	}

	void OnDestroy()
	{
		if (this.gameObject.GetComponent<Ennemy>().pv <= 0)
		{
			if (Random.Range(0, 3) == 2)
				Instantiate(bonus[Random.Range(0, bonus.Count)], this.transform.position, new Quaternion(0f,0f,180f,0f));
		}
	}

	public void pop_bonus()
	{
		pop = true;
	}

	void Update () 
	{
		if (pop)
		{
			range = Random.Range(1, bonus.Count) - 1;
			pop = false;
			Instantiate(bonus[range], this.transform.position, Quaternion.identity);
		}
	}

}
