﻿using UnityEngine;
using System.Collections;

public class Heal : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
		print ("test");
	}

	void OnCollisionEnter2D(Collision2D hit)
	{
		if (hit.gameObject.name == "Player")
		{
			if ((hit.gameObject.GetComponent<Healthbar>().curhealth += 5) > 100)
				hit.gameObject.GetComponent<Healthbar>().curhealth = 100;
			Destroy(this.gameObject);
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
