﻿using UnityEngine;
using System.Collections;

public class Healthbar : MonoBehaviour
{
	private int _curhealth = 100;
	private int maxhealth = 100;
	private int barlength;

	public Texture2D bgImage;
	public Texture2D fgImage;

	public GUISkin skin;
	
	void Start () 
	{
		barlength = bgImage.width;
	}

	void OnGUI () 
	{
		GUI.Box (new Rect (0,0, bgImage.width, bgImage.height), bgImage);
		GUI.Box (new Rect (25,4, ((barlength/100) * _curhealth), 42),"", skin.customStyles[0]);
	}
	
	public void AdjustHealth(int amount)
	{
			this._curhealth += amount;
			if (this._curhealth > this.maxhealth)
			{
				this._curhealth = this.maxhealth;
			}
			else if (this._curhealth < 0)
			{
				this._curhealth = 0;
			}
    }

	public int curhealth
	{
		get {return _curhealth;}
		set{_curhealth = value;}
	}
}
