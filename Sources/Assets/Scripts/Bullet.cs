﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour
{
	public Vector2 direction;
	public float speed;
	public float angle;

	// Use this for initialization
	void Start ()
	{
		Physics2D.IgnoreLayerCollision (LayerMask.NameToLayer ("Player"), LayerMask.NameToLayer ("Bullet"), true);
	}

	// Update is called once per frame

	void OnTriggerEnter2D (Collider2D hit)
	{

		if (hit != null) {

			if (hit.tag == "ground" || hit.tag == "platform") {
				Destroy (this.gameObject);
			}

			int pv;

			if (hit.gameObject.tag == "enemy") 
			{
				if (hit != null) {
					if (hit.gameObject.name == "EnnemyDistant")
						pv = hit.gameObject.GetComponent<EnemyDistant> ().pv--;
					else
						pv = hit.gameObject.GetComponent<Ennemy> ().pv--;
					if (pv <= 0) 
						Destroy (hit.gameObject);
					Destroy (this.gameObject);					
				}
			}
		}
	}

	void Update ()
	{
//		this.raycastdebug ();
		Vector3 bulletPositionViewport = Camera.main.WorldToViewportPoint (this.transform.position);
		//print ("BulletPositionViewport => " + bulletPositionViewport.ToString ());
		if (bulletPositionViewport.x > 1 || bulletPositionViewport.y > 1
		    || bulletPositionViewport.x < 0 || bulletPositionViewport.y < 0)
			Destroy (this.gameObject);
	}

	void FixedUpdate ()
	{
		this.rigidbody2D.velocity = new Vector2 (direction.x * speed, direction.y * speed);
	}

//	void OnCollisionEnter2D (Collision2D impact)
//	{
//		Destroy (this.gameObject);
//	}
	
}
