﻿using UnityEngine;
using System.Collections;

public class EnemyBullet : MonoBehaviour {

	public Vector2 direction;
	public float speed;
	public float angle;
	
	// Use this for initialization
	void Start ()
	{
	}
	
	// Update is called once per frame
	
	void OnTriggerEnter2D (Collider2D hit)
	{

//		print (hit.gameObject.name);
		if (hit != null) {
//			print ("EnemyBullet.tag = " + hit.tag);
			//						print ("Hit.tag = " + hit.tag);
			if (hit.tag == "ground" || hit.tag == "platform") {
				Destroy (this.gameObject);
			}
			
			int pv;
			
			if (hit.gameObject.tag == "Player") 
			{

//				print("Hit Player");
				if (hit != null) {
					hit.gameObject.GetComponent<Healthbar> ().AdjustHealth(-5);
					Destroy (this.gameObject);
				}
			}
		}
	}
	
	void Update ()
	{
		Vector3 bulletPositionViewport = Camera.main.WorldToViewportPoint (this.transform.position);
		//print ("BulletPositionViewport => " + bulletPositionViewport.ToString ());
		if (bulletPositionViewport.x > 1 || bulletPositionViewport.y > 1
		    || bulletPositionViewport.x < 0 || bulletPositionViewport.y < 0)
			Destroy (this.gameObject);
	}
	
	void FixedUpdate ()
	{
		this.rigidbody2D.velocity = new Vector2 (direction.x * speed, direction.y * speed);
	}
}
