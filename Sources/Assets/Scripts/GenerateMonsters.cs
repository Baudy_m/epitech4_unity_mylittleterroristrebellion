﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GenerateMonsters : MonoBehaviour {

	// Use this for initialization

	public GameObject ennemy;
	public List<GameObject> list_ennemy;

	public Move move;
	private GameObject tmp;
	private Vector3 v3Pos;

	void Start ()
	{
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (move.move_left && Random.Range(0,100) == 5)
		{
			v3Pos = Camera.main.ViewportToWorldPoint(new Vector3(1f, 0.3f, 14f));

			if (v3Pos.x > 0.5f)
			{
				v3Pos.z = 0f;
				v3Pos.y = 1f;

				tmp = Instantiate(ennemy, v3Pos, Quaternion.identity) as GameObject;
				list_ennemy.Add(tmp);
			}
		}

		if (move.move_right && Random.Range(0,100) == 5)
		{
			v3Pos = Camera.main.ViewportToWorldPoint(new Vector3(0f, 0.3f, 14f));
			v3Pos.z = 0f;
			v3Pos.y = 1f;
			tmp = Instantiate(ennemy, v3Pos, Quaternion.identity) as GameObject;
			list_ennemy.Add(tmp);
		}

		if (list_ennemy.Count > 0)
		{
			foreach (GameObject ennemy in list_ennemy.ToArray())
			{
				if (list_ennemy.Count == 0)
					break;

				Vector3 pos_ennemy = Camera.main.WorldToViewportPoint (ennemy.transform.position);

				if (pos_ennemy.x < 0 || pos_ennemy.x > 1)
				{
					GameObject tmp = ennemy;
					list_ennemy.Remove(ennemy);
					Destroy(tmp);
				}
			}
		}
	}
}
