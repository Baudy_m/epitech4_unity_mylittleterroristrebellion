﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour
{

		// Use this for initialization

		public GameObject original;
		public float delay;
		public float speed;
		public float delayElapse;
		public bool shooting;
		public float weaponAngle;
		private GameObject bullet;
		private Vector2 direction;

		void Start ()
		{
				this.direction = new Vector2 ();
				this.delayElapse = 0f;
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (Input.GetMouseButtonDown (0) || Input.GetButtonDown ("Fire3"))
						this.shooting = true;
				else if (Input.GetMouseButtonUp (0) || Input.GetButtonUp ("Fire3"))
						this.shooting = false;
				if (this.delayElapse <= 0f) {
						Vector3 mousePos = Input.mousePosition;
						mousePos.z = 0.0f;
						Vector3 lookPos = Camera.main.ScreenToWorldPoint (mousePos);
						lookPos = lookPos - transform.parent.position;
						this.weaponAngle = Mathf.Atan2 (lookPos.y, lookPos.x);
						this.direction.x = (Mathf.Cos (weaponAngle) * 1.0f - Mathf.Sin (weaponAngle) * 0.0f) * 1f;
						this.direction.y = (Mathf.Cos (weaponAngle) * 0.0f + Mathf.Sin (weaponAngle) * 1.0f) * 1f;
						this.weaponAngle *= Mathf.Rad2Deg;
//		Debug.DrawLine (this.transform.position, this.direction, Color.red);
//		this.direction.x = Camera.main.ScreenToWorldPoint (Input.mousePosition).x;
//		this.direction.y = Camera.main.ScreenToWorldPoint (Input.mousePosition).y;
//		Debug.DrawLine (this.transform.position, this.direction, Color.green);
						if (this.shooting)
								this.Shoot ();
						
				} else {
						this.delayElapse -= Time.deltaTime;
				}
		}
	
		void Shoot ()
		{
				this.delayElapse = this.delay;
				bullet = Instantiate (original, this.transform.parent.position, Quaternion.identity) as GameObject;
				Bullet bulletcollider = bullet.GetComponent<Bullet> ();
				bulletcollider.speed = this.speed;
				bulletcollider.direction = direction;
				bulletcollider.angle = this.weaponAngle;
				bulletcollider.transform.Rotate (0, 0, this.weaponAngle);
		}
	
}
